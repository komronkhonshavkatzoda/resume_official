const swiper = new Swiper('.swiper-container', {
  spaceBetween: 0,
  slidesPerView: 1,
  loop:   true,
  freeMode: false,
  watchSlidesProgress: true,
  navigation: {
    nextEl: ".swiper-next",
    prevEl: ".swiper-prev",
  },
  scrollbar: {
    el: '.swiper-scrollbar',
  },
});

let mybutton = document.getElementById("to_top");
window.onscroll = function () { scrollFunction() };
function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    mybutton.style.display = "block";
  } else {
    mybutton.style.display = "none";
  }
}
function topFunction() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}


let sliderButons = document.getElementsByClassName("group");

for (const element of sliderButons) {
  element.addEventListener("click", (event) => {
    for (let i = 1; i < 5; i++) {
      document.getElementById("colimg-" + i).style.display = 'none';
    }
    document.getElementById("colimg-" + event.currentTarget.getAttribute('data-id')).style.display = 'block';
  });
}


const TOKEN = "6662151061:AAGCig_IVdkuihslMc8x8NdgL2Dr7qYbylc";
const CHAT_ID = "-1001840438705";
const URI_API = `https://api.telegram.org/bot${TOKEN}/sendMessage`;

document.getElementById('send_tg').addEventListener('submit', function (e) {
  e.preventDefault();

  let message = `<b>Заявка с сайта!</b>\n`;
  message += `<b>Отправитель:</b> ${this.send_name.value}\n`;
  message += `<b>Почта отправителя:</b> ${this.send_email.value}\n`;
  message += `<b>Телефон отправителя:</b> ${this.send_tell_1.value}\n`;
  message += `<b>Телефон отправителя 2:</b> ${this.send_tell_2.value}\n`;
  message += `<b>Сообщение от пользователя:</b> ${this.description.value}`;



  axios.post(URI_API, {
    chat_id: CHAT_ID,
    parse_mode: 'html',
    text: message
  })
    .then((res) => {
      this.send_name.value = ""
      this.send_email.value = ""
      this.send_tell_1.value = ""
      this.send_tell_2.value = ""
      this.description.value = ""
      document.getElementById('message_sented').style.display = 'block'
    })
    .catch((err) => {
      console.warn(err);
    })
    .finilly(() => {
      console.log('End');
    })
})

document.getElementById('btn').addEventListener("click", function () {
  document.getElementById('work-list-2').style.display = "block",
  document.getElementById('btn').style.display = "none"
})

document.getElementById('buttn').addEventListener("click", function () {
  document.getElementById('work-list-2').style.display = "none",
  document.getElementById('btn').style.display = "block"
})

document.getElementById('intro').addEventListener("click", function(){
  document.getElementById("totop").style.display = "none"
})  